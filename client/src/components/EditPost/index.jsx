import React from 'react';
import PropTypes from "prop-types";
import { Modal, Input, Icon, Form, Button } from "semantic-ui-react";

import styles from "./styles.module.scss";

class EditPost extends React.Component {

  constructor(props) {
    super(props);

    const bodyMessage = this.props.post.body;
    this.state = {
      bodyMessage
    }
  }

  render() {
    const { post, onClose, onSubmit } = this.props;
    const { bodyMessage } = this.state;
    return (
      <Modal open onClose={onClose}>
        <Modal.Header className={styles.header}>
          Edit Post
        </Modal.Header>
        <Modal.Content>
          <Form onSubmit={() => onSubmit( {...post, body: bodyMessage} )}>

            <Form.TextArea
              style ={{minHeight: 300}}
              name="bodyMessage"
              value={bodyMessage}
              onChange={(event) => this.setState({ bodyMessage: event.target.value })} 
            />

            <Button color="green" type="submit">
              submit
            </Button>
            <Button color="grey" onClick={onClose}>
              Cancel
            </Button>
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  onClose: PropTypes.func.isRequired
}

export default EditPost;