import React from "react";
import PropTypes from "prop-types";
import { Card, Image, Label, Icon } from "semantic-ui-react";
import moment from "moment";

import styles from "./styles.module.scss";

const Post = ({ post, estimatePost, toggleExpandedPost, sharePost, isPersonalPost, setEditMode }) => {

    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt
    } = post;
    const date = moment(createdAt).fromNow();
    return (
        <Card style={{ width: "100%" }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by {user.username}
                        {" - "}
                        {date}
                    </span>
                    {isPersonalPost ?
                        <span>
                            <Label
                                className={styles.toolbarBtn + " right floated"}
                                basic
                                size="small"
                                as="a"
                                // onClick={() => setEditMode(post)}
                            >
                                <Icon name="delete" />
                            </Label>
                            <Label
                                className={styles.toolbarBtn + " right floated"}
                                basic
                                size="small"
                                as="a"
                                onClick={() => setEditMode(post)}
                            >
                                <Icon name="edit" />
                            </Label>
                            
                        </span>
                    : null}

                </Card.Meta>
                <Card.Description>{body}</Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => { estimatePost({ postId: id, isLike: true }) }}
                >
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => estimatePost({ postId: id, isLike: false })}
                >
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => toggleExpandedPost(id)}
                >
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => sharePost(id)}
                >
                    <Icon name="share alternate" />
                </Label>
            </Card.Content>
        </Card>
    );
};

Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    estimatePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

export default Post;
