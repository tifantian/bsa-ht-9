import postRepository from "../../data/repositories/post.repository";
import postReactionRepository from "../../data/repositories/post-reaction.repository";

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) =>
    postRepository.create({
        ...post,
        userId
    });

export const update = (userId = null, post) => {
    return postRepository.updateById(post.id, post);
}

export const setReaction = async (userId, { postId, isLike }) => {
    
    const updateOrDelete = react =>
        react.isLike === isLike
            ? postReactionRepository.deleteById(react.id)
            : postReactionRepository.updateById(react.id, { isLike });

    const reaction = await postReactionRepository.getPostReaction(
        userId,
        postId
    );

    const result = reaction
        ? await updateOrDelete(reaction)
        : await postReactionRepository.create({ userId, postId, isLike });

    const newReaction = await postReactionRepository.getPostReaction(
        userId,
        postId
    );

    return {
        reaction: newReaction,
        prevReaction: reaction
    };
};
